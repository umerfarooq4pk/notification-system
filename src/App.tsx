import React from 'react';
import { NotificationProvider } from './context/NotificationContext';
import Notifications from './components/Notifications';
import NotificationButtons from './components/NotificationButtons';

// Main App component to combine all parts
const App: React.FC = () => {
  return (
    <NotificationProvider>
      <div style={{ padding: '20px' }}>
        <h1>Notification App</h1>
        {/* Buttons to trigger notifications */}
        <NotificationButtons />
        {/* Display the list of notifications */}
        <Notifications />
      </div>
    </NotificationProvider>
  );
};

export default App;