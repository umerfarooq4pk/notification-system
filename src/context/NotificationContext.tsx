import React, { createContext, useState, ReactNode, useEffect } from 'react';
import { collection, addDoc, getDocs, updateDoc, doc } from "firebase/firestore";
import { db } from '../firebaseConfig';

// Define the type for a notification
type Notification = {
  id: string;
  message: string;
  read: boolean; 
};

// Define the type for the context
type NotificationContextType = {
  notifications: Notification[];
  addNotification: (message: string) => void;
  markAsRead: (id: string) => void;
};

// Create a context with the defined type
const NotificationContext = createContext<NotificationContextType | undefined>(undefined);

// Create a provider component for the context
const NotificationProvider: React.FC<{ children: ReactNode }> = ({ children }) => {
  const [notifications, setNotifications] = useState<Notification[]>([]);

  // Fetch notifications from Firestore on component mount
  useEffect(() => {
    const fetchNotifications = async () => {
      const querySnapshot = await getDocs(collection(db, "notifications"));
      const notificationsData = querySnapshot.docs.map(doc => ({
        id: doc.id,
        ...doc.data()
      })) as Notification[];
      setNotifications(notificationsData);
    };

    fetchNotifications();
  }, []);

  // Function to add a new notification to Firestore
  const addNotification = async (message: string) => {
    const newNotification = {
      message,
      read: false,
    };
    const docRef = await addDoc(collection(db, "notifications"), newNotification);
    setNotifications(prev => [...prev, { ...newNotification, id: docRef.id }]);
  };

  // Function to mark a notification as read in Firestore
  const markAsRead = async (id: string) => {
    const notificationDoc = doc(db, "notifications", id);
    await updateDoc(notificationDoc, { read: true });
    setNotifications(prev =>
      prev.map(notif => (notif.id === id ? { ...notif, read: true } : notif))
    );
  };

  return (
    <NotificationContext.Provider value={{ notifications, addNotification, markAsRead }}>
      {children}
    </NotificationContext.Provider>
  );
};

export { NotificationProvider, NotificationContext };