import React, { useContext } from 'react';
import { NotificationContext } from '../context/NotificationContext';

// Component to display the list of notifications
const Notifications: React.FC = () => {
  const context = useContext(NotificationContext);

  // Return null if context is not available
  if (!context) {
    return null;
  }

  const { notifications, markAsRead } = context;

  return (
    <div className='notifications_listing'>
        <div className='border-bottom flex-1'>
            {notifications.map(notification => (
                <div className='flex gap-10 items-center notification_item' key={notification.id}>
                <p>{notification.message}</p>
                {/* Display a button to mark the notification as read if it is unread */}
                {!notification.read && (
                    <button onClick={() => markAsRead(notification.id)}>Mark as Read</button>
                )}
                </div>
            ))}
        </div>
    </div>
  );
};

export default Notifications;