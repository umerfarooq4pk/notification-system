import React, { useContext } from 'react';
import { NotificationContext } from '../context/NotificationContext';

// Component with buttons to trigger different notifications
const NotificationButtons: React.FC = () => {
  const context = useContext(NotificationContext);

  // Return null if context is not available
  if (!context) {
    return null;
  }

  const { addNotification } = context;

  return (
    <div className='notifications_button'>
      <button onClick={() => addNotification('First Type Notification')}>First Notification Button</button>
      <button onClick={() => addNotification('Second Type Notification')}>Second Notification Button</button>
      <button onClick={() => addNotification('Third Type Notification')}>Third Notification Button</button>
    </div>
  );
};

export default NotificationButtons;